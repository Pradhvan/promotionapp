from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import RuleForm
from .models import Rule
from django.contrib.auth.decorators import login_required


def home(request):
    rules = Rule.objects.all()
    return render(request, 'home.html', {'rules': rules})


@login_required
def rule_form_view(request):
    form = RuleForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('home'))

    context = {
        'form': form
    }

    return render(request, 'rule_form.html', context)


@login_required
def rule_form_edit(request):
    form = RuleForm(instance=Rule)
    if request.method == 'POST':
        form = RuleForm(instance=Rule, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('home'))

    context = {
        'form': form
    }

    return render(request, 'rule_form.html', context)
