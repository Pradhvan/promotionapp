# Promotial
A web app for managing marketing campaigns. 


### Devlopment Setup: 

#### 1. Create & activate the virtual environment 

```Bash
python3 -m venv test
source test/bin/activate 
```
#### 2. Install the dependencies 

```BASH
pip install -r requirements.txt
```

#### 3. Create a super user & Run server: 

```BASH
python manage.py createsuperuser 
python manange.py runserver 
```